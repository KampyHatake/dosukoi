void ataca(long tiempoGiro, boolean sentidoGiro) {
  unsigned long temp = 0L;

  // Gira!
  temp = millis() + tiempoGiro;
  while (millis() <= temp)
    if (sentidoGiro)
      giroIzquierda();
    else
      giroDerecha();

  // Empuja!
  temp = millis() + 2000L;
  while (millis() <= temp)
    avanzar();

  // Firulais! busca!
  busca();
}

void busca(){
  long aleatorioIzquierda = 0L;
  long aleatorioDerecha = 0L;
  unsigned long tiempoBusqueda = 0L;

  enemigoDetectado();
  while(1) {
    leerSensores();
    aleatorioIzquierda = random(0, 256);
    aleatorioDerecha = random(0, 256);
    motores(aleatorioIzquierda, aleatorioDerecha, 0, 0);
    tiempoBusqueda = random(0, 1000) + millis();
    while (millis() <= tiempoBusqueda) {
      leerSensores();
      lineaDetectada();
      enemigoDetectado();
    }
  }
}

void enemigoDetectado() {
  unsigned long temp = 0L;

  if (sensorValue[2] <= 600) {
    temp = millis() + 2000L;
    while (millis() <= temp) {
      avanzar();
      leerSensores();
      lineaDetectada();
    }
  }
}

void lineaDetectada() {
  unsigned long temp = 0L;

  if ((sensorValue[1] <= 950) || (sensorValue[3] <= 950)) {
    temp = millis() + 300L;
    while(millis() <= temp)
      retroceder();
    temp = millis() + 250L;
    while(millis() <= temp)
      giroIzquierda();
  }
}
