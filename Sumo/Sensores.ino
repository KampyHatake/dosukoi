//sensorValue[1] = Izquierda
//sensorValue[2] = Frontal ~ 200
//sensorValue[3] = Derecha
void leerSensores() {
  sensorValue[1] = (analogRead(A1) + analogRead(A1)) / 2;
  sensorValue[2] = (analogRead(A2) + analogRead(A2)) / 2;
  sensorValue[3] = (analogRead(A3) + analogRead(A3)) / 2;

  if (DEBUG){
    Serial.print(sensorValue[1]);
    Serial.print(" ");
    Serial.print(sensorValue[2]);
    Serial.print(" ");
    Serial.print(sensorValue[3]);
    Serial.println("");
  }
}


void leerBotones() {
  boolean IZQUIERDA = false, DERECHA = true;
  boolean switch1 = true, switch2 = true, switch3 = true;
  boolean button = true, pulsado = false;

  button = digitalRead(bt1);
  
  while(!button){
    switch1 = digitalRead(sw1);
    switch2 = digitalRead(sw2);
    switch3 = digitalRead(sw3);
    button = digitalRead(bt1);
    pulsado = true;
  }

  if (pulsado) {
    delay(5000);

    // Enemigo a la izquierda!
    if (!switch1 &&  switch2 &&  switch3) ataca(80L, IZQUIERDA);
  
    // Enemigo enfrente!
    if ( switch1 && !switch2 &&  switch3) ataca(  0L, IZQUIERDA);
  
    // Enemigo detrás!
    if (!switch1 && !switch2 && !switch3) ataca(200L, IZQUIERDA);
  
    // Enemigo a la derecha!
    if ( switch1 &&  switch2 && !switch3) ataca(100L, DERECHA);
  }

  detener();
}
