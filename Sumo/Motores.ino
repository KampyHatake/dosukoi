void avanzar()        { motores(50, 255, 0, 0); }

void retroceder()     { motores(0, 0, 255, 50); }

void giroIzquierda() { motores(0, 255, 255, 0); }

void giroDerecha ()  { motores(255, 0, 0, 255); }

void detener ()       { motores(0, 0, 0, 0); }

void motores (int izquierdaDelante,
              int derechaDelante,
              int izquierdaAtras,
              int derechaAtras) {
  analogWrite (pin_motor_izquierdo_delante, izquierdaDelante);
  analogWrite (pin_motor_derecho_delante,   derechaDelante);
  analogWrite (pin_motor_izquierdo_detras,  izquierdaAtras);
  analogWrite (pin_motor_derecho_detras,    derechaAtras);
}
