// Alberto Ferreiro Campello "Kampy"
// Proyecto Sumo-Dosukoi
// http://oshwdem.org

// Optimización
#pragma GCC optimize("-O2")

// Librerías

// Pines de los motores
#define pin_motor_izquierdo_delante 10
#define pin_motor_izquierdo_detras   5
#define pin_motor_derecho_delante    6
#define pin_motor_derecho_detras     9

// Pines de los interruptores
#define sw1 4
#define sw2 3
#define sw3 2
#define bt1 11

// Pines de los sensores


// Constantes
const boolean DEBUG = false;

// Variables globales para lectura de sensores
volatile double sensorValue[6] = {0, 0, 0, 0, 0, 0};


void setup() {
  // Inicialización de pines
  DDRD |= B01100000; // Digitales 0~7 como entradas, 5 y 6 como salidas
  DDRB |= B00100110; // Digitales 8~13 como entradas, 9, 10, 12 y 13 como salidas
  DDRC |= B00000000; // Analogicos A0~A8 como entradas
  pinMode (sw1, INPUT_PULLUP);
  pinMode (sw2, INPUT_PULLUP);
  pinMode (sw3, INPUT_PULLUP);
  pinMode (bt1, INPUT_PULLUP);
  
  randomSeed(analogRead(0));

  PORTB &= ~B00110000; // 13 & 12 OFF
  if (DEBUG)
    Serial.begin(115200);
}


void loop() {
  leerSensores();
  leerBotones();
}
